var class_graph_tab_title =
[
    [ "__construct", "class_graph_tab_title.html#a095c5d389db211932136b53f25f39685", null ],
    [ "Set", "class_graph_tab_title.html#a332e43b310a08e5dd512008b5c5de9dc", null ],
    [ "SetColor", "class_graph_tab_title.html#a1e84a8ff73f808410b4e9dfe155cbb57", null ],
    [ "SetCorner", "class_graph_tab_title.html#a3ce975a93f92a0c287e1a43b7824f40d", null ],
    [ "SetFillColor", "class_graph_tab_title.html#a98562b5508fbff9d2136ce312fcdd61a", null ],
    [ "SetTabAlign", "class_graph_tab_title.html#a61af5fa992e37f0e04157a968ccd2717", null ],
    [ "SetWidth", "class_graph_tab_title.html#a711e9a7ce8048ad6e1d6cacedc8978c6", null ],
    [ "Stroke", "class_graph_tab_title.html#af30b8da373a8cdc1e89875967af3479c", null ],
    [ "$bordercolor", "class_graph_tab_title.html#ab65f44101bdd3bb4bc0dc764172467f3", null ],
    [ "$posx", "class_graph_tab_title.html#ac0d1c7920577ed77acdb27ae74dc40a6", null ],
    [ "$posy", "class_graph_tab_title.html#aaee839ef041a415ed4d4ad4abd1d6550", null ],
    [ "$width", "class_graph_tab_title.html#a5795120b4b324bc4ca83f1e6fdce7d57", null ]
];