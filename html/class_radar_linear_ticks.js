var class_radar_linear_ticks =
[
    [ "__construct", "class_radar_linear_ticks.html#a095c5d389db211932136b53f25f39685", null ],
    [ "GetMajor", "class_radar_linear_ticks.html#af952da89e3ebcde18b901137a5842fcb", null ],
    [ "GetMinor", "class_radar_linear_ticks.html#a2038ffbc9d20a78b29aefd30a5f851fa", null ],
    [ "Set", "class_radar_linear_ticks.html#a49e4bfdd4ce857f1012a606199d15562", null ],
    [ "Stroke", "class_radar_linear_ticks.html#a3186bb449a59053ba2043c0dcd0f93e0", null ],
    [ "$major_step", "class_radar_linear_ticks.html#ae7ade871d37ff0d60753e15e835782a6", null ],
    [ "$xtick_offset", "class_radar_linear_ticks.html#a6efa514cab66b7b6e619fc5f2c8f2621", null ]
];