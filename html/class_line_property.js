var class_line_property =
[
    [ "__construct", "class_line_property.html#a0129b3fa2addbb6426355a3d55c652f2", null ],
    [ "SetColor", "class_line_property.html#aeab541b7e0059d668fe11daf075f049c", null ],
    [ "SetStyle", "class_line_property.html#a8265a4e8d8e7f3699e302fed1322e2bc", null ],
    [ "SetWeight", "class_line_property.html#acb384c59f6acf2599c8d4c99074eee5c", null ],
    [ "Show", "class_line_property.html#ac610f1466ea4be230c8be7a20b6d2e57", null ],
    [ "Stroke", "class_line_property.html#aa592e3d9a9e897c845c45b86eca465c0", null ],
    [ "$iColor", "class_line_property.html#af1cd42929aa8f384901cd7c2c87e8cb8", null ],
    [ "$iShow", "class_line_property.html#a7e14989da2a07cdf68bb24cc0942f7a5", null ],
    [ "$iStyle", "class_line_property.html#a3e39197a9c23da8a4e24fce8ed3b873d", null ],
    [ "$iWeight", "class_line_property.html#a84fd1b496f234f8aea0959290676e437", null ]
];