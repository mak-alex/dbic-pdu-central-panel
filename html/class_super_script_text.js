var class_super_script_text =
[
    [ "__construct", "class_super_script_text.html#a9c6689bf59c441b3566e7825051d188b", null ],
    [ "FromReal", "class_super_script_text.html#ab38f2eb731b7b8e340302609d95b5651", null ],
    [ "GetFontHeight", "class_super_script_text.html#a08ae200fd6be33fd35b202c026b4b7c6", null ],
    [ "GetTextHeight", "class_super_script_text.html#a13fee07b574f101ec408f139bdfe896e", null ],
    [ "GetWidth", "class_super_script_text.html#a42f683a0b17636f75b5d619e30594fbb", null ],
    [ "Set", "class_super_script_text.html#ab94abc5997caa280900f037ac938b0a4", null ],
    [ "SetSuperFont", "class_super_script_text.html#afe016e68e0e6f35c6697eff26b563f76", null ],
    [ "Stroke", "class_super_script_text.html#ad6d42b4d4404069e8790ce75b133d008", null ],
    [ "$iSuperScale", "class_super_script_text.html#a9b97e7d9fa4711e6a7f19b982f968ac0", null ],
    [ "$iVertOverlap", "class_super_script_text.html#a98296f1f6af6fe1fec579bd79535c957", null ],
    [ "$sfont_size", "class_super_script_text.html#aa68b1c131c865e5db7568bae43ea48be", null ],
    [ "$sfont_style", "class_super_script_text.html#af413772dc7a82c7e6e1c2cc7cacdfe28", null ]
];