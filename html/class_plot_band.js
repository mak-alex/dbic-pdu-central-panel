var class_plot_band =
[
    [ "__construct", "class_plot_band.html#a7e0eea2107829701ebe9174db671373e", null ],
    [ "GetDir", "class_plot_band.html#a2f2c9994956d4d9306a288c02149d57d", null ],
    [ "GetMax", "class_plot_band.html#a8a56b5b628beb2ed02d2afe77c96fef9", null ],
    [ "GetMin", "class_plot_band.html#a8cb8e4ae2764b959e77e18e4e0f9e25e", null ],
    [ "PreStrokeAdjust", "class_plot_band.html#a64fd3b2b3b7b866cfeebfc2d7d38207d", null ],
    [ "SetDensity", "class_plot_band.html#ac076db3c5147535bf56cc4a58b91c21d", null ],
    [ "SetOrder", "class_plot_band.html#a484534fdfbbe2a4bfa97c8a0af7820d9", null ],
    [ "SetPos", "class_plot_band.html#a20b57a1dcc491cde8ca5626006fadc2f", null ],
    [ "ShowFrame", "class_plot_band.html#aade39d3bf44adaa81a8d351e7e665ecb", null ],
    [ "Stroke", "class_plot_band.html#a024d44fb51f25ff21cd22521f5b000c5", null ],
    [ "$depth", "class_plot_band.html#a0abb04e6a8a04e161eb4657f54848b18", null ],
    [ "$max", "class_plot_band.html#a9fb747ef5633c244639185a7fe54f6b0", null ],
    [ "$min", "class_plot_band.html#a56ba76399d12529c48ecea581ecd08cf", null ]
];