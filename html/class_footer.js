var class_footer =
[
    [ "__construct", "class_footer.html#a095c5d389db211932136b53f25f39685", null ],
    [ "SetMargin", "class_footer.html#a0fded6227353b62cadf555fb44dd0ed4", null ],
    [ "SetTimer", "class_footer.html#ae5d1f4a50e047933f8fe0a1157628dd9", null ],
    [ "Stroke", "class_footer.html#a3030cae7073536c7312dee2f1cba3b72", null ],
    [ "$center", "class_footer.html#aa052ac144e957028ab840f59c49fc621", null ],
    [ "$iBottomMargin", "class_footer.html#ab38a52a366d1fcaa0fbc4a2fdd4614f0", null ],
    [ "$iLeftMargin", "class_footer.html#a9c22abdb8efaa73bae4c0e00ae90ef5f", null ],
    [ "$iRightMargin", "class_footer.html#a7f1bc3b30a9b92abb035cbefae215cf7", null ],
    [ "$itimerpoststring", "class_footer.html#a5f1b51fb6b971005f8776c14404f65dc", null ],
    [ "$left", "class_footer.html#a8ce48148e988ab5ecd61213c1d460962", null ],
    [ "$right", "class_footer.html#a79e068e5169b6f084dcf0baf74137378", null ]
];