var class_contour_plot =
[
    [ "__construct", "class_contour_plot.html#aaae1b189795ca107c2241b6c73b2f2f8", null ],
    [ "Invertlegend", "class_contour_plot.html#a41d0f48bce3a6072a7bdce45bb0ac820", null ],
    [ "Legend", "class_contour_plot.html#aa00959b9c207066414ceebeec4cc57cc", null ],
    [ "Max", "class_contour_plot.html#a6e49068d7ae2880c0934cd0b4cad2dc2", null ],
    [ "Min", "class_contour_plot.html#af4569019a81b5cd4f5b1c362adb80b7e", null ],
    [ "PreScaleSetup", "class_contour_plot.html#acf18dc7a4ccf88c34fb5baf20f7c0a6b", null ],
    [ "SetInvert", "class_contour_plot.html#aa29714c6ce49f2e842014db726253a19", null ],
    [ "SetIsobarColors", "class_contour_plot.html#a0b017b9e0f47f3f8ad08991055aa8a8e", null ],
    [ "ShowLegend", "class_contour_plot.html#a8631ea7a7e8b896ca29e6c6d540dc62d", null ],
    [ "Stroke", "class_contour_plot.html#adaf13ac9eb40df4dd2b23bf64b37a849", null ],
    [ "UseHighContrastColor", "class_contour_plot.html#a0132e63b199c33ba4ee56cfec805ea01", null ],
    [ "$contourColor", "class_contour_plot.html#afc914b22e29d72898fdcc30c44d06e6c", null ],
    [ "$contourCoord", "class_contour_plot.html#a44e0256d087630063a0cfe602a1d392d", null ],
    [ "$contourVal", "class_contour_plot.html#a5bf9517338644499f378548f4d66ab44", null ],
    [ "$highcontrastbw", "class_contour_plot.html#ae2c1c8c18f8d3c147bef5572bb1e15a0", null ]
];