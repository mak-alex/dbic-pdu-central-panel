var class_grid =
[
    [ "__construct", "class_grid.html#adae9eba3d9f007cafbfb3ae3f8d0b2fe", null ],
    [ "DoStroke", "class_grid.html#acc599387a0b24a28b06122daf742ca55", null ],
    [ "SetColor", "class_grid.html#a163fdd595b1790dc69291d24e2a23e77", null ],
    [ "SetFill", "class_grid.html#a3f19ca33356683f34026db2ad5f23e2a", null ],
    [ "SetLineStyle", "class_grid.html#ab408e454494b7a47e8a1cf4052f54625", null ],
    [ "SetStyle", "class_grid.html#aef447abaa22107cd1a60c1edc2289e86", null ],
    [ "SetWeight", "class_grid.html#a23bd43eef1c377a003cab7711fe04838", null ],
    [ "Show", "class_grid.html#a0bccbb6805d581396ed130ad494a7ad0", null ],
    [ "Stroke", "class_grid.html#a5113b1d3dd3c2aac3955e12d60021b81", null ],
    [ "$fill", "class_grid.html#aef7198fa71eebd442e6fc0b83a10b0fe", null ],
    [ "$fillcolor", "class_grid.html#a674ad519efef9ff6b75d8f0e17c1f34d", null ],
    [ "$img", "class_grid.html#a56a9a0da0c26b3d3029cea47a6b9292f", null ],
    [ "$majorcolor", "class_grid.html#a0b57a26b32336e235a4f16aa9b7f9447", null ],
    [ "$majortype", "class_grid.html#acdc759a982cd728ac02dc0d4ec3c909d", null ],
    [ "$majorweight", "class_grid.html#aeaaad58e9d80ce96156d67d675a96929", null ],
    [ "$minorcolor", "class_grid.html#a48caa297eea004cd4c0d6021c5f4e004", null ],
    [ "$minortype", "class_grid.html#a2d15b75f9c3cad9964f039c1f1709164", null ],
    [ "$minorweight", "class_grid.html#ac5de00088c2811ec0106eb1f42f5ff6e", null ],
    [ "$scale", "class_grid.html#a8457a6fc43abd2acbbeae26b8b7aa79f", null ],
    [ "$show", "class_grid.html#a464d9509eae521fbcfbf9eda3f920677", null ],
    [ "$showMinor", "class_grid.html#a2a111fc84c0bbac706496e505c32d402", null ]
];