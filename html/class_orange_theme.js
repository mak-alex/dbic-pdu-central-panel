var class_orange_theme =
[
    [ "ApplyPlot", "class_orange_theme.html#af7ea4bf408180f5aef0bd41af37a8402", null ],
    [ "GetColorList", "class_orange_theme.html#a8f4bc0fa54ca92850eb121953d38e551", null ],
    [ "PreStrokeApply", "class_orange_theme.html#a62108af6e0ab41f4a9f3de45ba03c699", null ],
    [ "SetupGraph", "class_orange_theme.html#abbf39bbecb73e74a2fccab044636fd5d", null ],
    [ "SetupPieGraph", "class_orange_theme.html#a9b7440ca9a4fa864666518409838af6c", null ]
];