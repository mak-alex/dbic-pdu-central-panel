var class_rect_pattern =
[
    [ "__construct", "class_rect_pattern.html#a574ca68fd68c301ef56e4da466ffb5f9", null ],
    [ "SetBackground", "class_rect_pattern.html#a8427f552856d075b884cc6a51bd48702", null ],
    [ "SetDensity", "class_rect_pattern.html#ac076db3c5147535bf56cc4a58b91c21d", null ],
    [ "SetPos", "class_rect_pattern.html#a20b57a1dcc491cde8ca5626006fadc2f", null ],
    [ "ShowFrame", "class_rect_pattern.html#a420c5e81773321006c798f7954d99861", null ],
    [ "Stroke", "class_rect_pattern.html#a3030cae7073536c7312dee2f1cba3b72", null ],
    [ "$color", "class_rect_pattern.html#aac3430cb9e46a0a966f9e52fb5b0f2e5", null ],
    [ "$doframe", "class_rect_pattern.html#a6a05443cc8b239ccb633114a21633576", null ],
    [ "$iBackgroundColor", "class_rect_pattern.html#a8a24619bef9adcbc5c4db73a7615706d", null ],
    [ "$linespacing", "class_rect_pattern.html#a65a4019c01b2ed204815d2ae6ead62f8", null ],
    [ "$rect", "class_rect_pattern.html#ac96876ca64e670f400554fa4dca02fe4", null ],
    [ "$weight", "class_rect_pattern.html#aee5b436bc1b075512126d3577e6db7d3", null ]
];