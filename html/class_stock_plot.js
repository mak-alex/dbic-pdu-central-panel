var class_stock_plot =
[
    [ "__construct", "class_stock_plot.html#a1448b296c1d74b5f12916c265fa7d846", null ],
    [ "HideEndLines", "class_stock_plot.html#afbf7160c3c82336d98f44259d9c76090", null ],
    [ "ModBox", "class_stock_plot.html#a3b82ea23642b9c8391ba0c34b09a3ebd", null ],
    [ "PreStrokeAdjust", "class_stock_plot.html#a4a76df0084c44297ca8774f9ca041a82", null ],
    [ "SetColor", "class_stock_plot.html#a840ba5507a84627167520615af509c09", null ],
    [ "SetWidth", "class_stock_plot.html#a711e9a7ce8048ad6e1d6cacedc8978c6", null ],
    [ "Stroke", "class_stock_plot.html#adaf13ac9eb40df4dd2b23bf64b37a849", null ],
    [ "$iStockColor2", "class_stock_plot.html#a3cf2406a819b8c636b8d9b94ecec008c", null ],
    [ "$iStockColor3", "class_stock_plot.html#ac20fc753d58f9da10fe97354b78ed1fe", null ],
    [ "$iTupleSize", "class_stock_plot.html#a0e1638009e3b7794f7dcd8dbe18a180c", null ]
];