var class_canvas_scale =
[
    [ "__construct", "class_canvas_scale.html#ac9be6511429ec9e0c37516a9b4062449", null ],
    [ "Get", "class_canvas_scale.html#aa613ae1d8b4ce741bbfc3224f23af3d9", null ],
    [ "Set", "class_canvas_scale.html#a377b44aca09bf6f559436de54be96efe", null ],
    [ "Translate", "class_canvas_scale.html#a47f2c537ee114cc41a28f5a6fb386bc3", null ],
    [ "TranslateX", "class_canvas_scale.html#ae8331e94b11b37686592c7c534dbafc7", null ],
    [ "TranslateY", "class_canvas_scale.html#ad21099fe0826ca2235521d32e2738c50", null ],
    [ "$h", "class_canvas_scale.html#ab00da9d5d8bd5b6b944793e093860aff", null ],
    [ "$ixmax", "class_canvas_scale.html#a0eed73dab9426c74ed780a4df47d2e70", null ],
    [ "$iymax", "class_canvas_scale.html#a0b9cf99d2b57c40292009d6883d191ea", null ],
    [ "$iymin", "class_canvas_scale.html#acbb621bc8825856062afe2e583606816", null ]
];