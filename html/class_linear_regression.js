var class_linear_regression =
[
    [ "__construct", "class_linear_regression.html#acb05160707639be4d31d3081b06b77c8", null ],
    [ "Calc", "class_linear_regression.html#a825e66013a1291777989f9ed214dc173", null ],
    [ "GetAB", "class_linear_regression.html#acf55e9f54b13dced7cda90e3d769ce40", null ],
    [ "GetStat", "class_linear_regression.html#a26f05a921b4d7a1464bf2fb065ccdb1b", null ],
    [ "GetY", "class_linear_regression.html#ab2996316c3e3cf58054cba7e6d607038", null ],
    [ "$ia", "class_linear_regression.html#a3bfa2074706a5a97f701e6a4c732e69b", null ],
    [ "$iCorr", "class_linear_regression.html#a520c40144f664d4c15cee9d7b72e12dc", null ],
    [ "$iDet", "class_linear_regression.html#a5a5b68c7899a08a63635ea108745ae2c", null ],
    [ "$iStdErr", "class_linear_regression.html#aaf6c0c7139fafa9422f0db0904b5c82e", null ],
    [ "$iy", "class_linear_regression.html#aa76114376902bb90fe656ac2bc9f3eed", null ]
];