var class_scatter_plot =
[
    [ "__construct", "class_scatter_plot.html#a1448b296c1d74b5f12916c265fa7d846", null ],
    [ "Legend", "class_scatter_plot.html#aa00959b9c207066414ceebeec4cc57cc", null ],
    [ "SetImpuls", "class_scatter_plot.html#a77522f29339e99441963993fc63bcdb8", null ],
    [ "SetLinkPoints", "class_scatter_plot.html#a7a8005b2d1f7b2d2ffd8235c8777474a", null ],
    [ "SetStem", "class_scatter_plot.html#a0cf598c42c5a490281a226a06d0caff5", null ],
    [ "Stroke", "class_scatter_plot.html#adaf13ac9eb40df4dd2b23bf64b37a849", null ],
    [ "$link", "class_scatter_plot.html#a5d346e31b75d916e3bac9cb193bfc97f", null ],
    [ "$mark", "class_scatter_plot.html#a340025994489998568770173df51a91a", null ]
];