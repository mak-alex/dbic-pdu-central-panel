var class_linear_ticks =
[
    [ "__construct", "class_linear_ticks.html#a095c5d389db211932136b53f25f39685", null ],
    [ "_doAutoTickPos", "class_linear_ticks.html#ae48bb55cf6e5c25778710aa91b32386b", null ],
    [ "_doLabelFormat", "class_linear_ticks.html#a48afcf0a1d6f71aa4068b032d47cb6f1", null ],
    [ "_doManualTickPos", "class_linear_ticks.html#af71d9943c86b691e950a3db25d97ac1c", null ],
    [ "_StrokeTicks", "class_linear_ticks.html#ae2dead530640837372b421b8db3e2eb5", null ],
    [ "AdjustForDST", "class_linear_ticks.html#ace36c0f59ac6fc65c58ed12c8b01aec9", null ],
    [ "GetMajor", "class_linear_ticks.html#af952da89e3ebcde18b901137a5842fcb", null ],
    [ "GetMinor", "class_linear_ticks.html#a2038ffbc9d20a78b29aefd30a5f851fa", null ],
    [ "HaveManualLabels", "class_linear_ticks.html#a44c4d8673175f3dd1e38d6b15ce4500f", null ],
    [ "Set", "class_linear_ticks.html#a49e4bfdd4ce857f1012a606199d15562", null ],
    [ "SetMajTickPositions", "class_linear_ticks.html#a0ad1c500b0e4e538b21a880febef11aa", null ],
    [ "SetTextLabelStart", "class_linear_ticks.html#ad926a54fe8e0d183d7fb533d5c1e8e45", null ],
    [ "SetTickPositions", "class_linear_ticks.html#af68d0cd294394467ca17638a5899cd92", null ],
    [ "SetXLabelOffset", "class_linear_ticks.html#a266a604b638d63952cf7d4cefcfb4cc8", null ],
    [ "Stroke", "class_linear_ticks.html#ad97652c580f41f5796dd42045aa6a3f6", null ],
    [ "$iManualMinTickPos", "class_linear_ticks.html#a646f095fd0af68edf99504a5de6c6a2b", null ],
    [ "$iManualTickLabels", "class_linear_ticks.html#a1d61e2ae1c7f27f8cbc44fce34d64795", null ],
    [ "$major_step", "class_linear_ticks.html#ae7ade871d37ff0d60753e15e835782a6", null ],
    [ "$minor_step", "class_linear_ticks.html#a909de4c6905314c71f11e1aae0e24bfd", null ],
    [ "$xlabel_offset", "class_linear_ticks.html#a1c50ce6d332b82dc315f1c7bc2f8925c", null ],
    [ "$xtick_offset", "class_linear_ticks.html#a6efa514cab66b7b6e619fc5f2c8f2621", null ]
];