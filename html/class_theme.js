var class_theme =
[
    [ "__construct", "class_theme.html#a095c5d389db211932136b53f25f39685", null ],
    [ "ApplyGraph", "class_theme.html#a178774f6af16cf7aeb4a5f67b33a56ec", null ],
    [ "ApplyPlot", "class_theme.html#af7ea4bf408180f5aef0bd41af37a8402", null ],
    [ "GetColorList", "class_theme.html#a8f4bc0fa54ca92850eb121953d38e551", null ],
    [ "GetNextColor", "class_theme.html#a2a47763749cb1fb9dfdedecc40ddf2a9", null ],
    [ "GetThemeColors", "class_theme.html#a4389a5ddae6cbdc6d57576fc6121c369", null ],
    [ "PreStrokeApply", "class_theme.html#a62108af6e0ab41f4a9f3de45ba03c699", null ],
    [ "SetupPlot", "class_theme.html#aa55ffb01b47a2356e6adf3abeadd26ec", null ],
    [ "$color_index", "class_theme.html#a33f6a1744b6e7ecee1d238b1b341e838", null ]
];