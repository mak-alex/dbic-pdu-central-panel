var class_polar_graph =
[
    [ "__construct", "class_polar_graph.html#a78e24f44495031624fa8ed7c4acf49e9", null ],
    [ "GetPlotsMax", "class_polar_graph.html#ae6636a653afa5c06dd89dbdd424c74a4", null ],
    [ "Set90AndMargin", "class_polar_graph.html#a7912bccab6756c97addae24ff164a4b7", null ],
    [ "SetClockwise", "class_polar_graph.html#a743bd5954d7f38c3305100c80e9859c3", null ],
    [ "SetDensity", "class_polar_graph.html#ae2168227a1378d59eb7be13d7247f96d", null ],
    [ "SetPlotSize", "class_polar_graph.html#a855363710ac63ad5229fd80e1890dbae", null ],
    [ "SetScale", "class_polar_graph.html#a4b6bc3ad2aefd234503db9a3605442b3", null ],
    [ "SetType", "class_polar_graph.html#a280b9cd8b4ea4c8d545733026943bd78", null ],
    [ "Stroke", "class_polar_graph.html#ae366922f858f22475096f5e7f74baf58", null ],
    [ "$axis", "class_polar_graph.html#a7c48970938636326f5f91f08a58a3b40", null ],
    [ "$iType", "class_polar_graph.html#a7c04412d0ac77023f788f986ef87570b", null ],
    [ "$scale", "class_polar_graph.html#a8457a6fc43abd2acbbeae26b8b7aa79f", null ]
];