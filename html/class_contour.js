var class_contour =
[
    [ "__construct", "class_contour.html#a292355e64c5a370362e7243b638483fd", null ],
    [ "adjustDataPointValues", "class_contour.html#a814b6c1c30c385922b6586367ee5fb1f", null ],
    [ "CalculateColors", "class_contour.html#a8a2bcadea52c4b261084b730cc6a9e56", null ],
    [ "determineIsobarEdgeCrossings", "class_contour.html#a8f53e8082d69b1107a51558e34a3caf7", null ],
    [ "getCrossingCoord", "class_contour.html#aa4eab104fc0e7cf699a60680ff563928", null ],
    [ "getIsobars", "class_contour.html#ad6a7660362b4e092011e8a0cfb5bc60b", null ],
    [ "getMinMaxVal", "class_contour.html#a70311a139a68cc6447fa45bc64076d09", null ],
    [ "isobarHCrossing", "class_contour.html#adfbe6a5f34ccc749e8d75c16aaff1460", null ],
    [ "isobarVCrossing", "class_contour.html#aaa6564b7c5c1e158a1c68a29dce9778d", null ],
    [ "resetEdgeMatrices", "class_contour.html#a09d860c208e314b07c052a204efae7a4", null ],
    [ "SetInvert", "class_contour.html#aa29714c6ce49f2e842014db726253a19", null ],
    [ "UseHighContrastColor", "class_contour.html#a0132e63b199c33ba4ee56cfec805ea01", null ],
    [ "$highcontrastbw", "class_contour.html#ae2c1c8c18f8d3c147bef5572bb1e15a0", null ],
    [ "$isobarColors", "class_contour.html#a03da461052f7799e30ec4d0ced1b0dd7", null ],
    [ "$nbrRows", "class_contour.html#a04ea097299e479a88544acd9e8506775", null ],
    [ "$vertEdges", "class_contour.html#a5983b907eeb92f4b602ac4f1d4cf7d78", null ]
];