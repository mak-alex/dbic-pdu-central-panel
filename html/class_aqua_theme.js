var class_aqua_theme =
[
    [ "ApplyPlot", "class_aqua_theme.html#af7ea4bf408180f5aef0bd41af37a8402", null ],
    [ "GetColorList", "class_aqua_theme.html#a8f4bc0fa54ca92850eb121953d38e551", null ],
    [ "PreStrokeApply", "class_aqua_theme.html#a62108af6e0ab41f4a9f3de45ba03c699", null ],
    [ "SetupGraph", "class_aqua_theme.html#abbf39bbecb73e74a2fccab044636fd5d", null ],
    [ "SetupPieGraph", "class_aqua_theme.html#a9b7440ca9a4fa864666518409838af6c", null ],
    [ "$axis_color", "class_aqua_theme.html#ae8a32c76390d0f76ba65537d7191c716", null ],
    [ "$background_color", "class_aqua_theme.html#ab99599d1702f329ce472bcbf96846531", null ],
    [ "$font_color", "class_aqua_theme.html#a8f5292731d9a60a40764efd46d2c7a61", null ],
    [ "$grid_color", "class_aqua_theme.html#a5f271b1226dfa2b9f3dd734c3b440c5a", null ]
];