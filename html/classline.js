var classline =
[
    [ "_get_variable_list", "classline.html#aa1b282c0a389a0816ddfa87823e21626", null ],
    [ "add", "classline.html#a5718dddc9c96c76cbcea478ce593dedb", null ],
    [ "add_data_link_tip", "classline.html#aafe9df23a9b2bc7c265101ea867fa0bf", null ],
    [ "add_data_tip", "classline.html#a451aab5eee0abdf49eca1429a0f5453b", null ],
    [ "add_link", "classline.html#a98f0e0ea0841998a1b611c0ad642966f", null ],
    [ "key", "classline.html#ac96e56a54a595e1bdcee89f4e62252a5", null ],
    [ "line", "classline.html#a86bdb1d4266885c5d2d6cc5086e25750", null ],
    [ "toString", "classline.html#ad152bb5e768858617b5cc69c7864af8a", null ],
    [ "$_key", "classline.html#a535630cefaf93746423d63a93daa1099", null ],
    [ "$colour", "classline.html#a4086b1a341b7c8462a47fb1b25fd49ab", null ],
    [ "$data", "classline.html#a6efc15b5a2314dd4b5aaa556a375c6d6", null ],
    [ "$key", "classline.html#aa60b0284e0dfa2463495481cf11e3cf4", null ],
    [ "$key_size", "classline.html#ac63b7b734e12cc40d9d6318900f66e10", null ],
    [ "$line_width", "classline.html#ab059678922a8e0657f4581a1c98c3479", null ],
    [ "$tips", "classline.html#a3d8e50f2e20daa264cff6d84832e1de3", null ]
];