var searchData=
[
  ['has8bitchars',['has8bitChars',['../class_p_h_p_mailer.html#a00a5a1581e01bf5ae1a012b55a7f2b12',1,'PHPMailer']]],
  ['hasmultibytes',['hasMultiBytes',['../class_p_h_p_mailer.html#a6bb4300a48c25d9fdec7fddf4f8e2549',1,'PHPMailer\hasMultiBytes($str)'],['../class_p_h_p_mailer.html#ae3d2298350cc0fca4808efa8253d6da4',1,'PHPMailer\HasMultiBytes($str)']]],
  ['headerline',['headerLine',['../class_p_h_p_mailer.html#a76caf3b881a28504c1bebedd520004bd',1,'PHPMailer\headerLine($name, $value)'],['../class_p_h_p_mailer.html#a4e72c338b9daa2f01adfdac70aabcdca',1,'PHPMailer\HeaderLine($name, $value)']]],
  ['hello',['Hello',['../class_s_m_t_p.html#a9c7137e431d9c50581a0f995d590aeeb',1,'SMTP\Hello($host= &apos;&apos;)'],['../class_s_m_t_p.html#aa57f991acecc5b519b790fad49acbe78',1,'SMTP\hello($host= &apos;&apos;)']]],
  ['hmac',['hmac',['../class_s_m_t_p.html#ab64534872ef0e960ac18d1a383781830',1,'SMTP']]],
  ['html2text',['html2text',['../class_p_h_p_mailer.html#adcc957bbfc73453cc7f04e412603989d',1,'PHPMailer']]]
];
