var class_log_ticks =
[
    [ "GetMajor", "class_log_ticks.html#af952da89e3ebcde18b901137a5842fcb", null ],
    [ "IsSpecified", "class_log_ticks.html#a1039958d6daf7841e9415ea858a66836", null ],
    [ "LogTicks", "class_log_ticks.html#a560841af1932e64a2ac875d4414424d5", null ],
    [ "SetLabelLogType", "class_log_ticks.html#a1f7dff6d0b6cae93b1e817035387d0c1", null ],
    [ "SetTextLabelStart", "class_log_ticks.html#a4afe193f711479ab34848907a8209662", null ],
    [ "SetXLabelOffset", "class_log_ticks.html#a7cedbae062f95d84ad01697ee638b539", null ],
    [ "Stroke", "class_log_ticks.html#a98b608a99d76ac38a4838f5729747705", null ]
];