var class_digital_l_e_d74 =
[
    [ "__construct", "class_digital_l_e_d74.html#a545dcbb90c98bcea6d11aedf3bf1002c", null ],
    [ "_GetLED", "class_digital_l_e_d74.html#ab3d5da2dc95149940ca442b995eeb7e8", null ],
    [ "SetSupersampling", "class_digital_l_e_d74.html#a355b01e2443a640a34dcdebcbd25713b", null ],
    [ "Stroke", "class_digital_l_e_d74.html#a7a254ae0d2ce65d65f3bd00596313e6e", null ],
    [ "StrokeNumber", "class_digital_l_e_d74.html#a76c1a3f9ea51c03dcf3c419878dfb1df", null ],
    [ "$iColorSchema", "class_digital_l_e_d74.html#a85ca2f510822fcc74d2984cb0d4b7b8f", null ],
    [ "$iLED_Y", "class_digital_l_e_d74.html#a621d33508a291809a749b9cb75f3cdf3", null ],
    [ "$iLEDSpec", "class_digital_l_e_d74.html#a0b0ea10a7475fac525da9ed2bc48e9be", null ],
    [ "$iMarg", "class_digital_l_e_d74.html#a11f63940ea03ca9c6d9b74d846a1b27a", null ],
    [ "$iRad", "class_digital_l_e_d74.html#ada4930b0a5e9544b546d65b40081ab00", null ],
    [ "$iSuperSampling", "class_digital_l_e_d74.html#a4c399c14ded680fe57bdeaeb04a68a13", null ]
];