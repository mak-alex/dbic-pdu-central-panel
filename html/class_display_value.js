var class_display_value =
[
    [ "__construct", "class_display_value.html#a095c5d389db211932136b53f25f39685", null ],
    [ "ApplyFont", "class_display_value.html#ad085507f95127dc6d5c1dc9a74bf5be6", null ],
    [ "HideZero", "class_display_value.html#a0e204e74daea6613430d4e359e72237b", null ],
    [ "SetAlign", "class_display_value.html#aa9c36bc5167d1943dc3954de65bd7e9d", null ],
    [ "SetAngle", "class_display_value.html#a39492d0dd12a3f71170ca78dc6d62c5f", null ],
    [ "SetColor", "class_display_value.html#ab2a67b6260b186dbf5cbd359f9960811", null ],
    [ "SetFont", "class_display_value.html#a60ec17822d28990863340c80e8b08aa0", null ],
    [ "SetFormat", "class_display_value.html#a5375f3aab891f1a73e24b334cd956232", null ],
    [ "SetFormatCallback", "class_display_value.html#ae7c66e5886aae28ec873723ec405519e", null ],
    [ "SetMargin", "class_display_value.html#a5841f5361805be081eb8c5b3739d372e", null ],
    [ "Show", "class_display_value.html#a03475fcce540f96a4b57de455da70f17", null ],
    [ "Stroke", "class_display_value.html#a69ccadfd4af2e8f6cbe112ad8ff946fb", null ],
    [ "$format", "class_display_value.html#a1e6e4ea377a908a5407c435e9054e3a9", null ],
    [ "$fs", "class_display_value.html#ab75bcb2460bb5a4cab719a43a4433701", null ],
    [ "$fsize", "class_display_value.html#a18e9fc4615d70cb3bb34bde5b9795d66", null ],
    [ "$halign", "class_display_value.html#ae779f380122eac336df244a70e2766b2", null ],
    [ "$margin", "class_display_value.html#a3bcbb0dc2ef6299e52d39c45188f6bbc", null ],
    [ "$negcolor", "class_display_value.html#a87341107eac69e0f1255ae8321cceeaa", null ],
    [ "$negformat", "class_display_value.html#aa993490e3f0b87295a538f7e8437c539", null ],
    [ "$show", "class_display_value.html#a464d9509eae521fbcfbf9eda3f920677", null ],
    [ "$txt", "class_display_value.html#aafd3010c1ef87e07032e04966af858bd", null ],
    [ "$valign", "class_display_value.html#a4d86aa5f0775e9bea89ff99d236c181c", null ]
];