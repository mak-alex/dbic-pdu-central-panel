var class_gantt_v_line =
[
    [ "__construct", "class_gantt_v_line.html#a80c0d4b47fc78c1864eb506642fa5b35", null ],
    [ "SetDayOffset", "class_gantt_v_line.html#a4ec17c84ce9fd6afe54fa077f5977584", null ],
    [ "SetRowSpan", "class_gantt_v_line.html#ac48f6bf2de4574baa3d561655b1dc494", null ],
    [ "SetTitleMargin", "class_gantt_v_line.html#a8f920597bf6fefb616497aabd48b549e", null ],
    [ "SetWeight", "class_gantt_v_line.html#acb384c59f6acf2599c8d4c99074eee5c", null ],
    [ "Stroke", "class_gantt_v_line.html#a10c3b56dc4c4b78342bc6320302d5a23", null ],
    [ "$iDayOffset", "class_gantt_v_line.html#a0ba535c3dba39910030d0c2888ef2641", null ],
    [ "$iEndRow", "class_gantt_v_line.html#a88a5e9d857f94f89a55bebd92faece1a", null ],
    [ "$title_margin", "class_gantt_v_line.html#a1363b52fd4731097765aac3c24f0c9c7", null ]
];