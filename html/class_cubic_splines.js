var class_cubic_splines =
[
    [ "prepareCoords", "class_cubic_splines.html#a687af2d8a82e33864723c37ec562354b", null ],
    [ "processCoords", "class_cubic_splines.html#a9d1b39ff11b0d3641f5f74491e13440c", null ],
    [ "setInitCoords", "class_cubic_splines.html#afedb4e3e8d113153f34a92254e392244", null ],
    [ "$aCoords", "class_cubic_splines.html#aed11229dd3936e5d6acc871e280ad134", null ],
    [ "$aCrdX", "class_cubic_splines.html#a1ea251ae3e0847c61ff91f50b78cbca7", null ],
    [ "$aCrdY", "class_cubic_splines.html#a067343e6e2eb7d7ff10c8fd7822a8d05", null ],
    [ "$aSplines", "class_cubic_splines.html#aa44e9d9c301f7795e2fa283732a941dd", null ],
    [ "$iMaxX", "class_cubic_splines.html#a0debdbc0740232f85289004a04dcc7ac", null ],
    [ "$iMinX", "class_cubic_splines.html#afb57eaed2412dbca04ba626daacc225d", null ],
    [ "$iStep", "class_cubic_splines.html#a071bdc7eaca0e61f778a0903ec2158b7", null ]
];