var class_html2_text =
[
    [ "__construct", "class_html2_text.html#a19beb4123940e78cf455fc879b9be0ea", null ],
    [ "_build_link_list", "class_html2_text.html#af06d44c0c6b644be72aa9e5d3fe05d83", null ],
    [ "_convert", "class_html2_text.html#a0cbd0aa1e04989995494edb5bfec7776", null ],
    [ "_convert_blockquotes", "class_html2_text.html#a1beb9c3723e3ab5fffafa264ed34c783", null ],
    [ "_convert_pre", "class_html2_text.html#a9621a9ce02113bab079b9441e7a5dd42", null ],
    [ "_converter", "class_html2_text.html#ae9420af4232c16271b19678e4efa26f6", null ],
    [ "_preg_callback", "class_html2_text.html#a9acfd22ea6158fd4ea556dc14134e106", null ],
    [ "_preg_pre_callback", "class_html2_text.html#a3b8d2b07ef07dbd0a82fc0557dcb02fa", null ],
    [ "get_text", "class_html2_text.html#a15c52f678b5e2fe635f9bf101d845c3b", null ],
    [ "p", "class_html2_text.html#ad661f0ce96766f6eeeca6ba26701d87a", null ],
    [ "print_text", "class_html2_text.html#aba76dc1998dedad760729edd83e736f4", null ],
    [ "set_allowed_tags", "class_html2_text.html#a62d278fdc979a0de59c35d298b2c3e31", null ],
    [ "set_base_url", "class_html2_text.html#a51b3e5022d0886474c4686237a8c580d", null ],
    [ "set_html", "class_html2_text.html#a3171b0a523b8199d007e1bb8a496ad0c", null ],
    [ "$_converted", "class_html2_text.html#ae0d071b0e97acbebce14aa31446f982e", null ],
    [ "$_link_list", "class_html2_text.html#a01f30b327312d5f46553168c68cbe7d4", null ],
    [ "$_options", "class_html2_text.html#a1bebdc689c84eee59ad24c77e5531762", null ],
    [ "$allowed_tags", "class_html2_text.html#a0beeeb8bad11f6da7b4d66e93de9b123", null ],
    [ "$callback_search", "class_html2_text.html#a87c40e822047cea2b6a0c80f8536ceac", null ],
    [ "$ent_replace", "class_html2_text.html#ac0d63e65b2e7bc7437f7271957d02bf3", null ],
    [ "$ent_search", "class_html2_text.html#a496bf4193ad38f0542363006b359c87d", null ],
    [ "$html", "class_html2_text.html#a6f96e7fc92441776c9d1cd3386663b40", null ],
    [ "$pre_content", "class_html2_text.html#a0dfc559d5dd6155d88a9ef826372ed87", null ],
    [ "$pre_replace", "class_html2_text.html#a6d29baa69ee84c8f3e131bdc91735fd7", null ],
    [ "$pre_search", "class_html2_text.html#aac729a41ae77c91c19d2b863d26af0e9", null ],
    [ "$replace", "class_html2_text.html#aed6e7e83c8d3cbc4f6797f93bf4a602b", null ],
    [ "$search", "class_html2_text.html#ae549977dd05a017aa81372f6994bf955", null ],
    [ "$text", "class_html2_text.html#adf95f30eaafccead90ab5e2cdb55e9b9", null ],
    [ "$url", "class_html2_text.html#acf215f34a917d014776ce684a9ee8909", null ],
    [ "$width", "class_html2_text.html#a5795120b4b324bc4ca83f1e6fdce7d57", null ]
];