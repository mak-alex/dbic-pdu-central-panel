<!DOCTYPE html>
<?php
require_once("megad_class.php");
include("header.php");
$megad = new megad();
?>
<body>

<?php
if ( isset($_REQUEST['new']) && $_REQUEST['new'] == 1 )
{
	foreach ( $megad->conf as $key => $val )
	$new_id = $key;
	$new_id++;
	$new_rec = array($new_id => array("name" => "DBIC-PDU Kit", "ip" => "10.1.1.244", "pwd" => "password", "P0" => "OUT0", "P1" => "OUT1", "P2" => "OUT2", "P3" => "OUT3", "P4" => "OUT4", "P5" => "OUT5", "P6" => "OUT6", "P7" => "OUT7", "P8" => "ALL","R0" => "1200", "R1" => "1200", "R2" => "1200", "R3" => "1200", "R4" => "1200", "R5" => "1200", "R6" => "1200", "R7" => "1200","R8" => "1200" ));
	$megad->conf += $new_rec;
	$_REQUEST['id'] = $new_id;
	$megad->write_conf();
}	
elseif ( !empty($_REQUEST['name']) )
{
	foreach ( $megad->conf[$_REQUEST['id']] as $key => $val )
	$megad->conf[$_REQUEST['id']][$key] = $_REQUEST[$key];
	$megad->write_conf();
}

?>

<div id="page">
	<div id="header"><div style="font-size:12px;margin-left:50px;"><?php echo $title_main;?></div><center><? echo $configPage;?></center><div style="margin-top:-90px;"><img src="images/dbic.png" style="height:97px; width:78px;margin-left: 9px;margin-top:9px;position: absolute; top:0; left:0;"></div></div>
	<div id="content">
		<div style="z-index:10000;position: absolute;margin-top:100px; margin-right:10px; top:0; right:0;"><form action="change_language.php" method="post" name="change_lang" id='change_lang_form'>
		   <label><select name="language" id="change_lang" onChange="javascript:submit();">
			  <!-- список доступных языков -->
			  <option value="kz" <?php if ($_SESSION["lang"] == 'kz') { echo 'selected="selected"';}?>><? echo $kazakh;?></option>
			  <option value="en" <?php if ($_SESSION["lang"] == 'en') { echo 'selected="selected"';}?>><? echo $english;?></option>
			  <option value="ru" <?php if ($_SESSION["lang"] == 'ru') { echo 'selected="selected"';}?>><? echo $russian;?></option>
		   </select></label>
		</form></div>
		<form class="form add_device" action="edit.php">

			<?php

			echo "<input type=\"hidden\" name=\"id\" value=\"".$_REQUEST['id']."\">";
			
			echo '<p>';
				echo '<label for="name_device">'.$name_device.'</label>';
				echo "<a class=\"tooltip\" href=\"#\"><input class=\"in in_big\" type=\"text\" id=\"name_device\" name=\"name\" value=\"".$megad->conf[$_REQUEST['id']]['name']."\" /> ";
				echo '<span class="info">Введите наименование устройства!</span></a>';
			echo '</p>';

			echo '<p>';
				echo '<label for="name_device">'.$ip_device.'</label>';
				echo "<a class=\"tooltip\" href=\"#\"><input class=\"in in_big\" type=\"text\" id=\"name_device\" name=\"ip\" value=\"".$megad->conf[$_REQUEST['id']]['ip']."\" /> ";
				echo '<span class="info">Введите IP адрес для подключения к устройству!</span></a>';
			echo '</p>';

			echo '<p>';
				echo '<label for="name_device">'.$pass_device.'</label>';
				echo "<a class=\"tooltip\" href=\"#\"><input class=\"in in_big\" type=\"password\" id=\"name_device\" name=\"pwd\" value=\"".$megad->conf[$_REQUEST['id']]['pwd']."\" /> ";
				echo '<span class="info">Введите пароль для подключения к устройству!</span></a>';
			echo '</p>';
			
			for ( $i = 0; $i <=8; $i++ )
			{
				echo '<p>';
					echo '<label for="name_device">'.$title.$i.'</label>';
					echo "<a class=\"tooltip\" href=\"#\"><input class=\"in in_big\" type=\"text\" id=\"name_device\" name=\"P$i\" value=\"".$megad->conf[$_REQUEST['id']]["P".$i]."\" />";
					echo "<input class=\"in in_big\" type=\"text\" id=\"porog_outlets\" name=\"R$i\" maxlength=\"4\" size=\"4\" value=\"".$megad->conf[$_REQUEST['id']]["R".$i]."\" />";
					echo '<span class="info">Введите наименование а также установите порог для розетки ID:'.$i.'</span></a> ';
					//echo '<span class="info">Введите наименование для розетки ID:'.$i.'</span></a>';
				echo "</p>";
			}			
			?>
			<p align=right><input class="button save" type="submit" value="<? echo $save;?>" /><input type="button" class="button" value="<? echo $back;?>" onclick="history.back()"></p>
			
		</form>
		
	</div>
	<!-- ?pg=2&a=a&name=&mac=84-85-88-16-0-48&ip=10.1.1.246&mask=255.255.255.0&gateway=0&pwd=password-->
	<nav id="menu">
		<ul>
		<?php echo $megad->menu(); ?>
		</ul>
	</nav>
</div>
<div id="footer_e"><a href="http://dbic.pro/" target="_blank">Design Bureau of Industrial Communication</a> © 2014 </div>

</body>
</html>
