<!DOCTYPE html>
<?php
require_once("megad_class.php");
include("header.php");
$megad = new megad();
GLOBAL $megad;
?>
<div id="toggle-box">

	<div id="header"><div style="font-size:12px;margin-left:50px;"><?php echo $title_main;?></div><center><? echo $title_manual;?></center><div style="margin-top:-130px;"><img src="images/dbic.png" style="height:97px; width:78px;margin-left: 9px;margin-top:9px;position: absolute; top:0; left:0;"></div></div>
	<div id="content">
	<div style='width:410px;'>
	<input class="toggle-box" id="identifier-1" type="checkbox" >
	<label for="identifier-1">Как добавить новое устройство?</label>
	<div class="tooltip_msg faq_msg"><b>Добавить</b> новое устройство можно из меню "<b>Добавить устройство</b>", после чего вы будите перенаправленны на страницу конфигурирования устройства. </div>

	<input class="toggle-box" id="identifier-2" type="checkbox" >
	<label for="identifier-2">Как поменять наименование устройства или розетки?</label>
	<div class="tooltip_msg faq_msg">Для смены наименования устройства или розетки, перейдите в нужное устройство, после чего нажмите на кнопку "<b>Изменить</b>", после чего вы будите переадресованны на страницу конфигурирования устройства.</div>

	<input class="toggle-box" id="identifier-3" type="checkbox" >
	<label for="identifier-3">Как сменить сетевые настройки и пароль устройства?</label>
	<div class="tooltip_msg faq_msg">Чтобы сменить сетевые настройки или пароль, вам нужно авторизоваться напрямую по IP адресу устройства, после чего зайти в меню "<b>Настройки</b>".</div>

	<input class="toggle-box" id="identifier-4" type="checkbox" >
	<label for="identifier-4">Как происходит сбор статистика?</label>
	<div class="tooltip_msg faq_msg">Сбор статистики происходит каждый час, после чего формируется за весь день.</div>
	
	<input class="toggle-box" id="identifier-5" type="checkbox" >
	<label for="identifier-5">Где хранится статистика?</label>
	<div class="tooltip_msg faq_msg">Статистика находится в каталоге "<b>protocol</b>".</div>
	
	</div>
	</div>
	<nav id="menu">
		<ul>
		<?php echo $megad->menu(); ?>
		</ul>
	</nav>
</div>
<script>
<?
?>