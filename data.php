<?
// Подключаем библиотеку
include ("libs/jpgraph/jpgraph.php");
include ("libs/jpgraph/jpgraph_line.php");

// Пишем SQL-запрос к базе
// Структура базы может быть разной, поэтому заменю его тестовыми значениями
$ydata = array("2.5", "2.7", "2.8", "3.0");
$xdata = array("12:00", "12:05", "12:10", "12:15");

$graph = new Graph(450,200,"auto");
$graph->SetScale("textlin");
$graph->SetMarginColor('white');
$graph->SetFrame(true,'#B3BCCB', 1);
$graph->SetTickDensity(TICKD_DENSE);
$graph->img->SetMargin(50,20,20,60);
$graph->title->SetMargin(10);
$graph->xaxis->SetTickLabels($xdata);
$graph->xaxis->SetLabelAngle(90);
$graph->xaxis->SetPos('min');

// Обычно значений много, не менее 250 в сутки, 
// поэтому нельзя выводить все значения из массива $xdata на шкалу X
// Это будет сильным нагромождением, поэтому я вывожу каждое 30-е значение.
$my_interval = 30;
$graph->xaxis->SetTextTickInterval($my_interval);

$lineplot=new LinePlot($ydata);
$graph->Add($lineplot);
$graph->Stroke();

?>