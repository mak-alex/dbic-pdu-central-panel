<!DOCTYPE html>
<?php
require_once("megad_class.php");
include("header.php");

?>
<body>

<div id="page">
	<div id="header"><div style="font-size:12px;margin-left:50px;"><?php echo $title_main;?></div><center><? echo $menu_main; ?></center><img src="images/dbic.png" style="height:97px; width:78px;margin-left: 9px;margin-top:9px;position: absolute; top:0; left:0;"></div>
	<div id="content">
		<p><? echo $main_msg; ?></p>
		<center><img src="images/tip_ePDU.jpg"></center>
		<!-- форма для смены языка -->
		<div style="z-index:10000;width:200px;position: absolute;margin-top:100px; margin-right:10px; top:0; right:0;">
			<form action="change_language.php" method="post" name="change_lang" id='change_lang_form'>
			   <select name="language" id="change_lang" data-native-menu="false" data-mini="true" onChange="javascript:submit();">
				  <!-- список доступных языков -->
				  <option value="kz" <?php if ($_SESSION["lang"] == 'kz') { echo 'selected="selected"';}?>><? echo $kazakh;?></option>
				  <option value="en" <?php if ($_SESSION["lang"] == 'en') { echo 'selected="selected"';}?>><? echo $english;?></option>
				  <option value="ru" <?php if ($_SESSION["lang"] == 'ru') { echo 'selected="selected"';}?>><? echo $russian;?></option>
			   </select></label>
			</form>
		</div>
	</div>
	<!--<?
	include_once 'open_flash_chart_object.php';
$baseURL = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
open_flash_chart_object( 900, 250, $baseURL.'/chart-data.php', false, $baseURL );
?>-->
	<nav id="menu">
		<ul>
		<?php echo $megad->menu(); ?>
		</ul>
	</nav>
</div>
<div id="footer_e"><a href="http://dbic.pro/" target="_blank">Design Bureau of Industrial Communication</a> © 2014 </div>
</body>
</html>
