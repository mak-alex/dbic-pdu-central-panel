<!DOCTYPE html>
<?php

require_once("megad_class.php");
include("header.php");
$megad = new megad();

$check_fault = 0;
if ( $megad->demo == "false" )
{
	// Проверяем доступность устройства
	@$fp = fsockopen($megad->conf[$_GET['id']]['ip'],80,$errno,$errstr,1);
	if ( !$fp )
	{
		$check_fault = 1;
		$megad->demo = "true";
	} 
	else
	fclose($fp);
}
if ( !empty($_REQUEST['del']) && !empty($_REQUEST['id']) )
{
	unset($megad->conf[$_REQUEST['id']]);
	$megad->write_conf();
}
?>
<body>
<div id="page">
	<div id="header"><a href="#menu"></a><?php echo $megad->conf[$_GET['id']]['name']; echo " | Текущее время - ".date("H:i:s");?></div>
	<div id="content">
<?php
if ( $megad->demo != "true" )
		{
			$state = file_get_contents("http://".$megad->conf[$_GET['id']]['name']."/stat");
			$pieces = preg_split("/[\s:]+/", $state);	
			$k=0;
			for ( $i = 9; $i <= 16; $i++ ) { 
				$state = "ADC ".$k.": ";
				$state .= $pieces[$i];
				$state .= '<br />';
				echo $state;
				$k++;
		}
} else {
		if ( $check_fault == 1 )
		echo "<h2>Ошибка! Не удалось установить соединение с устройством по адресу: ".$megad->conf[$_GET['id']]['ip']."</h2>";
		if ( $megad->demo == "true" )
		echo '<h3>Включен демо-режим</h3>';
		for ( $i = 0; $i <= 7; $i++ ) {
			$state = "ADC ".$i.": ";
			$state .= "0";
			$state .= '<br />';
			echo $state;
		}
}
?>
</div>
	<nav id="menu">
		<ul>
		<?php echo $megad->menu(); ?>
		</ul>
	</nav>
</div>

</body>
</html>
