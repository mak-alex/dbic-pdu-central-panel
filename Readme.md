# DBIC-PDU Central Panel
** Централизованная панель управления устройствами DBIC-PDU
-----------------------------------
### Возможности
* добавить неограниченное кол-во устройств DBIC-PDU
* редактировать настройки неограниченного кол-ва устройств DBIC-PDU
-----------------------------------
### Скриншоты
-------------
![Alt text](https://bytebucket.org/d_e_n_i_e_d/dbic-pdu-central-panel/raw/ec611180fcdfb93b7d23dd8e22863d69d628b43b/screenshots/manipulation_dbic-pdu.png?token=bae5d35e624c9c5535f86223c13dc761f2324fe2)