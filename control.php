<!DOCTYPE html>
<?php
require_once("megad_class.php");
include("header.php");
$megad = new megad();

$date_file = date('Y-m-d');
$timeNow = date('H:i:s');
$check_fault = 0;
if ( $megad->demo == "false" )
{
	// Проверяем доступность устройства
	@$fp = fsockopen($megad->conf[$_GET['id']]['ip'],80,$errno,$errstr,1);
	if ( !$fp )
	{
		$check_fault = 1;
		$megad->demo = "true";
	} 
	else
	fclose($fp);
}

?>
<body>

<script type="text/javascript" src="libs/itoggle.jquery.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	var id = <?php echo $_GET['id']; ?>;
	var my_toggle = $('.iToggle').iToggle(
	{
		callback_func: function(data)
		{
			$.get("megad-ajax.php",
			{
				id : id,
				port : data.attr("port"),
				state : data.prop("checked"),
			}, function(data2){} );
		}
	}
	);
	
	$('#all_on').click( function()
	{
		$.get("megad-ajax.php",	{ id : id, all : "on" });
		my_toggle.trigger("iToggle.toggleAll", "on");
	});
	
	$('#all_off').click( function()
	{
		$.get("megad-ajax.php",	{ id : id, all : "off" });
		my_toggle.trigger("iToggle.toggleAll", "off");
	});
	
	<?php if ( $megad->update_time > 0 && $megad->demo != "true" ) { ?>
	setInterval(function(){
	$.get('megad-ajax.php', {id: id, get_all : 1}, function(data)
	{
		state_all = data.split(';');
		//alert(state_all);
		for ( i = 0; i <= 8; i++ )
		my_toggle.trigger("iToggle.toggleID", ["p" + i, state_all[i]]);
	})
	}, <?php echo $megad->update_time * 1000;?> );
	<?php } ?>
	
});
</script>
<script type="text/javascript">

$(document).ready(
function()
{
    // Отправляем лог на почту каждые 5 минут
    var timer = window.setInterval(
    function()
    {
	$('#log_in').attr('src', 'statistics.php');
    }, 300000);
}
)
</script>

<div id="page">

	<div id="header"><div style="font-size:12px;margin-left:50px;"><?php echo $title_main;?></div><center></a><?php echo $megad->conf[$_GET['id']]['name'];?></center><div style="margin-top:-130px;"><img src="images/dbic.png" style="height:97px; width:78px;margin-left: 9px;margin-top:9px;position: absolute; top:0; left:0;"></div></div>
	<div id="content">
		<?php
		if ( $check_fault == 1 )
		echo "<div class=\"tooltip_warn critical\">".$err_msg.$megad->conf[$_GET['id']]['ip']."</div>";
		if ( $megad->demo == "true" )
		echo "<div class=\"tooltip_warn critical\" style='margin-bottom:160px;'>".$demo_msg."</div>";
		//echo '<h3>'.$demo_msg.'</h3>';
		?>
		<? //echo $type_msg;?> 
		<div style="z-index:10000;position: absolute;margin-top:100px; margin-right:10px; top:0; right:0;"><form action="change_language.php" method="post" name="change_lang" id='change_lang_form'>
		   <label><select name="language" id="change_lang" onChange="javascript:submit();">
			  <!-- список доступных языков -->
			  <option value="kz" <?php if ($_SESSION["lang"] == 'kz') { echo 'selected="selected"';}?>><? echo $kazakh;?></option>
			  <option value="en" <?php if ($_SESSION["lang"] == 'en') { echo 'selected="selected"';}?>><? echo $english;?></option>
			  <option value="ru" <?php if ($_SESSION["lang"] == 'ru') { echo 'selected="selected"';}?>><? echo $russian;?></option>
		   </select></label>
		</form></div>
		
		<form class="form add_device" action="">
			<?php
				for ( $i = 0; $i <= 8; $i++ )
				{
					$state = $megad->get($_GET['id'], $i);
					if ( preg_match("/^ON/", $state) ) {
						$checked = ' checked="checked"';
					} else {
						$checked = '';
					}
					
					if ( $megad->demo != "true" )
					{
						GLOBAL $megad,$log_pieces,$k;
						$state = file_get_contents("http://".$megad->conf[$_GET['id']]['ip']."/stat");
						$pieces = preg_split("/[\s:]+/", $state);	
						$k=9;
						//for ( $i = 9; $i <= 16; $i++ ) { 
						//$state = "";
						$state = $pieces[$k];
						$state .= 'W<br />';
						$log_pieces[$k]= "ADC ".$i.": ";
						$log_pieces[$k].=$pieces[$k];
					}
					//log_input_data($log_pieces[$k]);
					
					echo "<p><input type=\"checkbox\" class=\"iToggle \" id=\"p$i\" data-label-on=\"on\" port=\"$i\" data-label-off=\"off\" value=\"1\" $checked/> <b>".$megad->conf[$_GET['id']]["P".$i]."</b></p>";
				}
					 
			?>
			
		</form>
		
		<!--img src="graph.php"-->
		<!--a href="#" id="all_on" class="button play"><? echo $all_on;?></a><a href="#" id="all_off" class="button star"><? echo $all_off;?></a--><?php echo "<a href=\"edit.php?id=".$_GET['id']."\" class=\"button edit\">".$edit."</a>"; ?><?php echo "<a href=\"stat.php?id=".$_GET['id']."\" class=\"button star\">".$statistic."</a>"; ?>
		<?
		
					// echo "<div ><center><b>Текущая нагрузка</b></center>";
					 echo "<table title='".$date_file."::".$timeNow."' style=\"position: absolute;top:210px;right:100px;\" cellpadding=\"2\" cellspacing=\"1\" width=\"490px;\" border=\"0\">";
					 echo "<tr><td><b>ID</b></td>";
					 echo "<td><b>IP</b></td>";
					 echo "<td><b>".$stat_date."</b>";
					 echo "</td><td><b>".$stat_time."</b></td>";
					 echo "<td><b>".$stat_name."</b></td>";
					 echo "<td><b>".$stat_power."</b></td></tr>";
					 for ( $i = 0; $i <= 7; $i++ ) {			
						 $state = $megad->get($_GET['id'], $i);
						 if ( preg_match("/^ON/", $state) ) {
							$checked = ' checked="checked"';
						 } else {
							$checked = '';
						 }
					
						 if ( $megad->demo != "true" ) {
							GLOBAL $megad,$log_pieces,$k;
							$state = file_get_contents("http://".$megad->conf[$_GET['id']]['ip']."/stat");
							$pieces = preg_split("/[\s:]+/", $state);	
							$k=9;
							//for ( $i = 9; $i <= 16; $i++ ) { 
							//$state = "";
							$state = $pieces[$k];
							$state .= 'W<br />';
							$log_pieces[$k]= "ADC ".$i.": ";
							$log_pieces[$k].=$pieces[$k];
							if ( $pieces[$k]>= $megad->conf[$_REQUEST['id']]["R".$i]) {
								echo "<span class=\"tooltip_warn warning_outlets\" style=\"border-radius: 5px 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; \">Превышено напряжение!<br />На розетке ".$i." - ".$state."<b>".$megad->conf[$_REQUEST['id']]["R".$i]."</span>";
							}
						 }
						 echo "<tr><td>$i</td>";
						 echo "<td >".$megad->conf[$_GET['id']]['ip']."</td>";
						 echo "<td >$date_file</td>";
						 echo "<td >$timeNow</td>";
						 echo "<td >".$megad->conf[$_GET['id']]["P".$i]."</td>";
						 if ($state > 0) {
							echo "<td >$state</td></tr>";
						 } else {
							echo "<td>0</td></tr>";
						 }
					 }
					 echo "</table>";
		?>
	</div>
	
	<nav id="menu">
		<ul>
		<?php echo $megad->menu(); ?>
		</ul>
	</nav>
</div>
<div id="footer_e"><a href="http://dbic.pro/" target="_blank">Design Bureau of Industrial Communication</a> © 2014 </div>

</body>
</html>
