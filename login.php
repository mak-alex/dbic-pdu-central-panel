<?php
//session_start();
if(!isset($_SESSION['member'])) {
    echo "<form action=\"login.php\" method=\"post\">";
    echo "<input type=\"text\" name=\"login\"><br />";
    echo "<input type=\"password\" name=\"password\"><br />";
    echo "<input type=\"submit\" value=\"log in!\"><br />";
    echo "</form>";
    die("<b>Please Authorise...</b>");
} else if(isset($_POST['login']) and isset($_POST['password'])) {
    $line = file('users.dat');
    $c = sizeof($line);
    for($i = 0; $i < $c; $i++) {
        $ex = explode('|', trim($line[$i]));
        if(($ex[1] == $_POST['login']) and ($ex[2] == md5($_POST['password'])) and ($ex[3] <= PAGE_ACCESS_LEVEL)) {
            $member = $ex;
            session_register('member');
            header('location:/'.$_SERVER['PHP_SELF']);
        }
    }
    die('Authentication failed...');
}
?>