<?php

require_once("config.php");

class megad extends conf
{
	public $conf = array();
	public $id = 0;

	public function __construct()
	{
		$conf_file = "dev_conf.json";
		$dev_conf = fopen($conf_file, "r");
		$conf_json = fread($dev_conf, filesize($conf_file));
		fclose($dev_conf);
	
		$this->conf = json_decode($conf_json, TRUE);
		
		if ( !empty($_REQUEST['id']) )
		$this->id = $_REQUEST['id'];
		
	}
	
	public function write_conf()
	{
		$json = json_encode($this->conf);
		$dev_conf = fopen("dev_conf.json", "w");
		fwrite($dev_conf, $json);
		fclose($dev_conf);
	}
	
	public function menu()
	{
	
		GLOBAL $_REQUEST,$menu_main, $menu_contacts, $menu_manual, $menu_add;
		$result = '<li><a href="index.php" class="brak transition"> '.$menu_main.'</a></li>';
		$result .= '<li><a href="contacts.php" class="brak transition"> '.$menu_contacts.'</a></li>';
		$result .= '<li><a href="manual.php" class="brak transition"> '.$menu_manual.'</a></li>';
		$result .= '<li><a href="edit.php?new=1" class="add transition"> '.$menu_add.'</a></li>';
		foreach ( $this->conf as $key => $val )
		{
			if ( $this->id == $key ) $selected = ' class="Selected"'; else $selected = "";
			$result .= "<li><a class='star' href=\"control.php?id=$key\" width=\"80px\" class=\"edit\"> ".$this->conf[$key]['name']."</a></li><div style='position:absolute;margin-top:-38px;right:10px;'><form action='index.php' method='get'>";
			$result .= "<input type=\"hidden\" name=\"id\" value=\"".$key."\"><input name=\"del\" class=\"buttdel\" type=\"submit\" value=\"x\" /></form></div>";
		}
		
		
		return $result;
	}
	
	public function control($id, $port, $state)
	{
		if ( $this->demo != "true" )
		file_get_contents("http://".$this->conf[$id]['ip']."/".$this->conf[$id]['pwd']."/?pg=1&n=$port&sw=$state");
				//http://10.1.1.244/password/?pg=1&n=0&sw=0
	}
	
	public function get($id, $port)
	{
		if ( $this->demo != "true" )
		{	
			$state = file_get_contents("http://".$this->conf[$id]['ip']."/stat");
			$pieces = preg_split("/[\s:]+/", $state);	
			for ( $i = 0; $i <= 7; $i++ ) { 
			   if (($port == $i)&&($pieces[$i] == 0)) {
			       $state = "ON";
			   }
			}
			return $state;	
		}
		return;
	}
	/*public function adc($id, $port)
	{
		if ( $this->demo != "true" )
		{	
			$adc_state = file_get_contents("http://".$this->conf[$id]['ip']."/stat");
			$pieces = preg_split("/[\s:]+/", $adc_state);	
			for ( $i = 8; $i <= 14; $i++ ) { 
			   if (($port == $i)&&($pieces[$i] == 0)) {
			       $adc_state = $port;
			       $adc_state .= ":";
			       $adc_state .= $i;
			       $adc_state .= "ON";
			   }
			}
			return $adc_state;	
		}
		return;
	}*/
}
?>