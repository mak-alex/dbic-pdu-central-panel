<?php setlocale(LC_TIME, 'rus'); // имя локали в Windows?>
<head>
<meta charset="utf-8">
<meta name="apple-mobile-web-app-capable" content="no" />
<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
<script type='text/javascript' charset='utf-8'>
    // Hides mobile browser's address bar when page is done loading.
      window.addEventListener('load', function(e) {
        setTimeout(function() { window.scrollTo(0, 1); }, 1);
      }, false);
</script>
<title></title>

<link type="text/css" rel="stylesheet" href="f/main.css" />
<link type="text/css" rel="stylesheet" href="f/jquery.mmenu.css" />
<link type="text/css" rel="stylesheet" href="f/extensions/jquery.mmenu.widescreen.css" media="all and (min-width: 900px)" />
<link type="text/css" rel="stylesheet" href="f/extensions/jquery.mmenu.themes.css" media="all and (min-width: 900px)" />
<link rel="stylesheet" type="text/css" href="f/engage.itoggle.css"/>

<style type="text/css">
	@media all and (min-width: 900px) {
	html, body {height: 100%;}
	#menu {background: #333;}
	#page {border-left: 1px solid #3332;min-height: 100%;}
	a[href="#menu"] {display: none !important;}
	}
</style>

<script type="text/javascript" src="libs/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="libs/jquery.mmenu.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
      	$("body").css("display", "none");
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
            $("body").css("display", "none");
      	    $("body").fadeIn(2000);
    });
</script>

<script type="text/javascript">
	$(function() {
		$('nav#menu').mmenu({
			classes: 'mm-black'
		});
	});
</script>

 
</head>
<?
session_start();
// Проверяем, имеются ли языковые переменные. Если нет - создаем их.
if (!isset($_SESSION["lang"]))
{
    if (!isset($_COOKIE["lan"])) // Проверяем язык браузера, объявляем языковые переменные.
    {
       $langs=explode('-',$_SERVER['HTTP_ACCEPT_LANGUAGE']);
       $lang=preg_replace('/;.*$/','',$langs[0]);
       $lang=preg_replace('/[^a-zA-Z_\-]/','',$lang);

       $cookie_set = setcookie ("lan", $lang, time()+60*60*24*30);
       $_SESSION["lang"] = $lang;
    }
    else
    {
       $_SESSION["lang"] = $_COOKIE["lan"];
    }
}

// Подключаем словарь для PHP-скриптов
if ($_SESSION["lang"] == 'kz') {
    require_once('lang/kz.php');
    $js_dict = 'kz';
} elseif ($_SESSION["lang"] == 'en') {
    require_once('lang/en.php');
    $js_dict = 'en';
} elseif ($_SESSION["lang"] == 'ru') {
    require_once('lang/ru.php');
    $js_dict = 'ru';
}
$megad = new megad();
if ( !empty($_REQUEST['del']) && !empty($_REQUEST['id']) ) {
	unset($megad->conf[$_REQUEST['id']]);
	$megad->write_conf();
}
?>
