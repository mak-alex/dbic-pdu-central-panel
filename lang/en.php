<?
$title_main = "Address of production and administration:<br />Respublika Kazakhstan, Almaty St Capljina 71<br /><br /><b>Tel.</b> +7-727-234-41-45<br /><b>Email:</b> gosvideotv@dbic.pro<br /><br />";
$main_msg = "<b>DBIC-Panel</b> - This is a simple interface that allows you to manage devices <b>DBIC-PDU </b> and get information about the current state of the port. <br />
For the interface does not require Database. <br />
Left in the menu, you can select an existing or add a new device. Ports are configured here.";
/* ------------- BUTTON PAGE LANG ---------------- */
$all_on = "All on";
$all_off = "All off";
$edit = "Edit";
$statistic = "Statistics";
$save = "Save";
$delete = "Delete";
$back = "Back";
$submitStat = "Send";
$configPage = "Configure device";
/* ------------- MENU LANG ---------------- */
$menu_main = "Main";
$menu_contacts = "Contacts";
$menu_manual = "FAQ";
$menu_add = "Add";
/* ------------- MAIN PAGE LANG ---------------- */
$err_msg = "Error! Unable to establish connection to the device at: ";
$demo_msg = "Included demo mode";
$type_msg = "Current status of all ports. Power consumption at the moment.";
/* ------------- CONTACT PAGE LANG ---------------- */
$contact_title = "Contacts";
$contacts = "<b>LLP \" KB PROMSVYAZ \"</b>	Registered address:<br />
The Republic of Kazakhstan<br />
Almaty, Alatau village , st. Ibragimov d.9<br />
Industrial Technology Park \"Alatau\" in Special Economic Zone<br /><br />

Address of production and administration:<br />
The Republic of Kazakhstan<br />
Almaty, st. Chaplin 71<br />
<b>Tel.</b> +7-727-234-41-45<br />
<b>Email:</b> <a href='mailto:gosvideotv@dbic.pro'>gosvideotv@dbic.pro</a><br /><br />

<a href='http://www.dbic.pro' target='_blank'>www.dbic.pro</a><br />
Help Desk: +7.716.293.4111 ( 8:00 - 17:00)<br />
+7.705.412.9562 (Around the clock)<br />
Skype: help.desk2014<br />
E-mail: hd@kbps.kz";
/* ------------- EDIT PAGE LANG ---------------- */
$name_device = "Name device";
$ip_device = "IP address device";
$pass_device = "Password";
$title = "Title ";
/* ------------- STAT PAGE LANG ---------------- */
$stat_title = "Статистика";
$stat_date = "Date";
$stat_time = "Time";
$stat_name = "Name";
$stat_power = "Power";
/* ------------- LANG SWITCH LANG ---------------- */
$kazakh="Kazakh";
$english = "English";
$russian = "Russian";
/* ------------ MANUAL PAGE LANG ------------------*/
$title_manual = "FAQ";
?>