<!DOCTYPE html>
<?php
require_once("megad_class.php");
include("header.php");

$megad = new megad();
$date_file = date('Y-m-d');
GLOBAL $fileName,$value;
$fileName=$value;
$maxStat=700; //количество записей, отображаемых
$headerColor="#808080";
$headerFontColor="#000";
$fontFace="Arial, Times New Roman, Verdana";
$fontSize="1";
$tableColor="#000000";
$rowColor="#CECECE";
$fontColor="#0000A0";
$textFontColor="#000000";
function showStat () {
					 GLOBAL $kazakh,$english,$russian,$date_file,$submitStat,$fileName,$back,$value,$stat_date,$stat_name,$stat_time,$stat_power,$title_main,$megad, $headerColor, $headerFontColor, $fontFace, $fontSize, $tableColor, $fileName, $maxStat, $rowColor, $fontColor, $textFontColor;
					 //вывожу таблицу
					 $fbase=file('protocol/'.$fileName);
					 $fbase = array_reverse($fbase);
					 //$count = sizeOf($fbase);
					 echo "<script>
					 function getCookie(name) {
						var matches = document.cookie.match(new RegExp(
						  \"(?:^|; )\" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + \"=([^;]*)\"
						))
						return matches ? decodeURIComponent(matches[1]) : undefined
					 }       
					 function setCookie(name, value, props) {
						props = props || {}
						var exp = props.expires
						if (typeof exp == \"number\" && exp) {
							var d = new Date()
							d.setTime(d.getTime() + exp*1000)
							exp = props.expires = d
						}
						if(exp && exp.toUTCString) { props.expires = exp.toUTCString() }
 
						value = encodeURIComponent(value)
						var updatedCookie = name + \"=\" + value
						for(var propName in props){
							updatedCookie += \"; \" + propName
							var propValue = props[propName]
							if(propValue !== true){ updatedCookie += \"=\" + propValue }
						}
						document.cookie = updatedCookie
 
					}
            
					function fn(){
						var elem= document.getElementsByName('sel')[0]
						var currentOptionIndex= elem.selectedIndex;
						setCookie('OptionIndex', currentOptionIndex);
					}
            
            
					window.onload=function(){
						var elem= document.getElementsByName('sel')[0];
						elem.selectedIndex=getCookie('OptionIndex') || 1;
					}
					</script>";
					 ?>
					 <div style="z-index:10000;position: absolute;margin-top:100px;top:0; right:0;"><form action="change_language.php" method="post" name="change_lang" id='change_lang_form'>
					   <label><select name="language" id="change_lang" onChange="javascript:submit();">
						  <!-- список доступных языков -->
						  <option value="kz" <?php if ($_SESSION["lang"] == 'kz') { echo 'selected="selected"';}?>><? echo $kazakh;?></option>
						  <option value="en" <?php if ($_SESSION["lang"] == 'en') { echo 'selected="selected"';}?>><? echo $english;?></option>
						  <option value="ru" <?php if ($_SESSION["lang"] == 'ru') { echo 'selected="selected"';}?>><? echo $russian;?></option>
					   </select></label>
					</form></div>
					 <?
					 echo "<div id=\"page\">";
					 echo '<div id="header"><div style="font-size:12px;margin-left:50px;">'.$title_main.'</div><center>Статистика</center><div style="margin-top:-130px;"><img src="images/dbic.png" style="height:97px; width:78px;margin-left: 9px;margin-top:9px;position: absolute; top:0; left:0;"></div></div>';
					 echo "<div id=\"content\">";
					 
					 //echo "<a href='#' onclick=\"history.back()\" class=\"button star\">".$back."</a>";
					 //echo "<font face=\"$fontFace\" color=\"$textFontColor\" size=\"$fontSize\">";
					 echo "<div align=\"center\"><table cellpadding=\"2\" cellspacing=\"1\" width=\"95%\" border=\"0\">";
					 echo "<tr bgcolor=\"$headerColor\"><td><font face=\"$fontFace\" color=\"$headerFontColor\" size=\"$fontSize\">IP</font></td>";
					 echo "<td><font face=\"$fontFace\" color=\"$headerFontColor\" size=\"$fontSize\">".$stat_date."</font>";
					 echo "</td><td><font face=\"$fontFace\" color=\"$headerFontColor\" size=\"$fontSize\">".$stat_time."</font></td>";
					 echo "<td><font face=\"$fontFace\" color=\"$headerFontColor\" size=\"$fontSize\">".$stat_name."</font></td>";
					 echo "<td><font face=\"$fontFace\" color=\"$headerFontColor\" size=\"$fontSize\">ID</font></td>";
					 echo "<td><font face=\"$fontFace\" color=\"$headerFontColor\" size=\"$fontSize\">".$stat_power."</font></td></tr>";
					 echo "</font><!--font face=\"$fontFace\" size=\"$fontSize\"-->";
					 if($h = opendir('protocol')) {
						 while(($f = readdir($h)) !== false) {
								if(($f != '.') && ($f != '..') && ($f != 'stat.php')) {
										$a[]=$f;
								}
						 }
						 if($a != '') {
								rsort($a);
								echo '<div style=\'position:relative;margin-right:40px;right:0;\'><form method="post" action=""><select name=sel size=1 onchange=\'fn()\'">';
								foreach ($a as $value) {
										echo '<option value="protocol/'.$value.'" />'.$value;
								}
								echo '</select> <input type="submit" class="button" value="'.$submitStat.'"></form></div>';
								closedir($h);
						 }
					 }
					 $option = isset($_POST['sel']) ? $_POST['sel'] : false;
					 echo $option;
					 if(empty($option)) {
						$fileName = "protocol/".$date_file."_log.log";
						if( !file_exists($fileName)) {
							$fp = fopen($fileName, "w"); // ("r" - считывать "w" - создавать "a" - добовлять к тексту), мы создаем файл
							fwrite($fp, "");
						fclose ($fp);
						}
					 } else {
						$fileName = $option;
					 }
					 
					 //открываю файл и запускаю цикл
					 $fbase=file($fileName);
					 //$fbase = array_reverse($fbase);
					 for ($i=0; $i<=$maxStat; $i++) :
						 if ($i>= sizeof($fbase)) { break; }
						 $s = $fbase[$i];
						 
					 //вывожу данные
					 $ip = $megad->conf[$_GET['id']]['ip'];
					 //echo " ".$ip." ";
					 
					 $onlin = $fileName;
					 $ipcount = file($onlin);
					 
					 foreach($ipcount as $i) {
						$i = explode(' ', $i);
					    $ip_from_file = $i[0];
						if(strcmp($ip, $ip_from_file) == 0) {
							 echo "<tr><td >$i[0]</font>";
							 echo "</td><td >$i[1]</font>";
							 echo "</td><td >$i[2]</font>";
							 echo "</td><td >$i[3]</font>";
							 echo "</td><td >$i[4]</font>";
							 echo "</td><td >$i[5]</font>";
							 echo "</td></tr>";
						 }
					 }
					 
					 //echo '<div id="chart_div" style="width: 900px; height: 200px;"></div>';
					 endfor;
					 echo "</table><br /><br /></div></div><nav id='menu'>";
					 echo "		<ul>";
					 echo $megad->menu();
					 echo "		</ul>";
					 echo "	</nav>";
					 echo '<div id="footer_e"><a href="http://dbic.pro/" target="_blank">Design Bureau of Industrial Communication</a> © 2014 </div>';
					 echo "</div></body></html>";
}

showStat();
?>